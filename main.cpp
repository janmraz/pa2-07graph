#ifndef __PROGTEST__
#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <vector>
#include <queue>
#include <stack>
#include <deque>
#include <map>
#include <set>
#include <list>
#include <algorithm>

#if defined ( __cplusplus ) && __cplusplus > 199711L /* C++ 11 */
#include <unordered_map>
#include <unordered_set>
#include <memory>
#include <functional>
#endif /* C++ 11 */

using namespace std;
#endif /* __PROGTEST__ */

class CTrain;

class NoRouteException
{
};
template <typename _T, typename _E>
struct Edge {
    _T from;
    _T to;
    const _E * train;
    bool visited;
    Edge(const _T & from, const _T & to, const _E& train): from(from),to(to), train(&train), visited(false) {

    }
    void SetVisited(bool state){
       visited = state;
    }
    bool GetVisited(){
        return visited;
    }
};
template <typename _T, typename _E>
struct Node {
    vector<shared_ptr<Edge<_T,_E>>> m_edges;
    const _T * m_name;
    bool m_visited;

    Node(const _T & name);

    Node();

    void AddEdge(shared_ptr<Edge<_T,_E>> ptr);
};
template <typename _T, typename _E>
Node<_T,_E>::Node(const _T &name): m_name(&name), m_visited(false) {

}
template <typename _T, typename _E>
Node<_T,_E>::Node(): m_visited(false) {

}

template <typename _T, typename _E>
void Node<_T,_E>::AddEdge(shared_ptr<Edge<_T,_E>> ptr) {
    m_edges.push_back(ptr);
}

template <typename _T,typename _E>
class CRoute
{
    map<_T,Node<_T,_E>> m_nodes;
    list<shared_ptr<Edge<_T,_E>>> m_edges;
    void clearVisited();
  public:
    CRoute<_T,_E> & Add(const _T& u1,const _T& u2,const _E & ctr);

    list<_T> Find(const _T& u1,const _T& u2);

    template <typename Func>
    list<_T> Find(const _T& u1,const _T& u2, Func fun);
};

template <typename _T,typename _E>
CRoute<_T,_E> &CRoute<_T,_E>::Add(const _T &u1, const _T &u2, const _E &ctr) {
    m_edges.push_back(make_shared<Edge<_T,_E>>(u1,u2,ctr));
    m_nodes.insert(make_pair(u1,Node<_T,_E>(u1)));
    m_nodes.insert(make_pair(u2,Node<_T,_E>(u2)));
    m_nodes.at(u1).AddEdge(m_edges.back());
    m_nodes.at(u2).AddEdge(m_edges.back());
    return *this;
}
template <typename _T,typename _E>
list<_T> CRoute<_T,_E>::Find(const _T &u1, const _T &u2) {
    if(u1 == u2)
        return { u1 };
    if(!m_nodes.count(u1))
        throw NoRouteException();
    if(!m_nodes.count(u2))
        throw NoRouteException();
    queue<pair<Node<_T,_E> *,list<_T>>> lookQue;

    clearVisited();

    auto node1 = m_nodes[u1];
    lookQue.push(make_pair(&node1,list<_T>()));
    lookQue.front().second.push_back(u1);

    while(!lookQue.empty()){
        auto pair1 = lookQue.front();

        if(*pair1.first->m_name == u2){
            return pair1.second;
        }

        for( const auto& edge : pair1.first->m_edges) {
            if(!edge->GetVisited()){
                edge->SetVisited(true);
                _T to = edge->from == *pair1.first->m_name ? edge->to : edge->from;

                list<_T> new_list = pair1.second;
                new_list.push_back(to);

                lookQue.push(make_pair(&m_nodes[to], new_list));
            }
        }

        lookQue.pop();
    }

    throw NoRouteException();
}
template <typename _T, typename _E>
void CRoute<_T,_E>::clearVisited() {
    for(const auto & edge: m_edges)
        edge->SetVisited(false);
}
template <typename _T,typename _E>
template <typename Func>
list<_T> CRoute<_T,_E>::Find(const _T &u1, const _T &u2, Func fun) {
    if(u1 == u2)
        return { u1 };
    if(!m_nodes.count(u1))
        throw NoRouteException();
    if(!m_nodes.count(u2))
        throw NoRouteException();
    queue<pair<Node<_T,_E> *,list<_T>>> lookQue;

    clearVisited();

    auto node1 = m_nodes[u1];
    lookQue.push(make_pair(&node1,list<_T>()));
    lookQue.front().second.push_back(u1);

    while(!lookQue.empty()){
        auto pair1 = lookQue.front();

        if(*pair1.first->m_name == u2){
            return pair1.second;
        }

        for( const auto& edge : pair1.first->m_edges) {
            if(!edge->GetVisited() && fun(*edge->train)){
                edge->SetVisited(true);
                _T to = edge->from == *pair1.first->m_name ? edge->to : edge->from;

                list<_T> new_list = pair1.second;
                new_list.push_back(to);

                lookQue.push(make_pair(&m_nodes[to], new_list));
            }
        }

        lookQue.pop();
    }

    throw NoRouteException();
}


#ifndef __PROGTEST__
//=================================================================================================
class CTrain
{
  public:
                             CTrain                        ( const string    & company,
                                                             int               speed )
                             : m_Company ( company ),
                               m_Speed ( speed )
    {
    }
    //---------------------------------------------------------------------------------------------
    string                   m_Company;
    int                      m_Speed;
};
//=================================================================================================
class TrainFilterCompany
{
  public:
                             TrainFilterCompany            ( const set<string> & companies )
                             : m_Companies ( companies )
    {
    }
    //---------------------------------------------------------------------------------------------
    bool                     operator ()                   ( const CTrain & train ) const
    {
      return m_Companies . find ( train . m_Company ) != m_Companies . end ();
    }
    //---------------------------------------------------------------------------------------------
  private:
    set <string>             m_Companies;
};
//=================================================================================================
class TrainFilterSpeed
{
  public:
                             TrainFilterSpeed              ( int               min,
                                                             int               max )
                             : m_Min ( min ),
                               m_Max ( max )
    {
    }
    //---------------------------------------------------------------------------------------------
    bool                     operator ()                   ( const CTrain    & train ) const
    {
      return train . m_Speed >= m_Min && train . m_Speed <= m_Max;
    }
    //---------------------------------------------------------------------------------------------
  private:
    int                      m_Min;
    int                      m_Max;
};
//=================================================================================================
bool               NurSchnellzug                           ( const CTrain    & zug )
{
  return ( zug . m_Company == "OBB" || zug . m_Company == "DB" ) && zug . m_Speed > 100;
}
//=================================================================================================
static string      toText                                  ( const list<string> & l )
{
  ostringstream oss;

  auto it = l . cbegin();
  oss << *it;
  for ( ++it; it != l . cend (); ++it )
    oss << " > " << *it;
  return oss . str ();
}
//=================================================================================================
int main ( void )
{
  CRoute<string,CTrain> lines;

  lines . Add ( "Berlin", "Prague", CTrain ( "DB", 120 ) )
        . Add ( "Berlin", "Prague", CTrain ( "CD",  80 ) )
        . Add ( "Berlin", "Dresden", CTrain ( "DB", 160 ) )
        . Add ( "Dresden", "Munchen", CTrain ( "DB", 160 ) )
        . Add ( "Munchen", "Prague", CTrain ( "CD",  90 ) )
        . Add ( "Munchen", "Linz", CTrain ( "DB", 200 ) )
        . Add ( "Munchen", "Linz", CTrain ( "OBB", 90 ) )
        . Add ( "Linz", "Prague", CTrain ( "CD", 50 ) )
        . Add ( "Prague", "Wien", CTrain ( "CD", 100 ) )
        . Add ( "Linz", "Wien", CTrain ( "OBB", 160 ) )
        . Add ( "Paris", "Marseille", CTrain ( "SNCF", 300 ))
        . Add ( "Paris", "Dresden",  CTrain ( "SNCF", 250 ) );

  list<string> r1 = lines . Find ( "Berlin", "Linz" );
  assert ( toText ( r1 ) == "Berlin > Prague > Linz" );
  list<string> r2 = lines . Find ( "Linz", "Berlin" );
  assert ( toText ( r2 ) == "Linz > Prague > Berlin" );

  list<string> r3 = lines . Find ( "Wien", "Berlin" );
  assert ( toText ( r3 ) == "Wien > Prague > Berlin" );

  list<string> r4 = lines . Find ( "Wien", "Berlin", NurSchnellzug );
  assert ( toText ( r4 ) == "Wien > Linz > Munchen > Dresden > Berlin" );

  list<string> r5 = lines . Find ( "Wien", "Munchen", TrainFilterCompany ( set<string> { "CD", "DB" } ) );
  assert ( toText ( r5 ) == "Wien > Prague > Munchen" );

  list<string> r6 = lines . Find ( "Wien", "Munchen", TrainFilterSpeed ( 120, 200 ) );
  assert ( toText ( r6 ) == "Wien > Linz > Munchen" );

  list<string> r7 = lines . Find ( "Wien", "Munchen", [] ( const CTrain & x ) { return x . m_Company == "CD"; } );
  assert ( toText ( r7 ) == "Wien > Prague > Munchen" );

  list<string> r8 = lines . Find ( "Munchen", "Munchen" );
  assert ( toText ( r8 ) == "Munchen" );

  list<string> r9 = lines . Find ( "Marseille", "Prague" );
  assert ( toText ( r9 ) == "Marseille > Paris > Dresden > Berlin > Prague"
           || toText ( r9 ) == "Marseille > Paris > Dresden > Munchen > Prague" );

  try
  {
    list<string> r10 = lines . Find ( "Marseille", "Prague", NurSchnellzug );
    assert ( "Marseille > Prague connection does not exist!!" == NULL );
  }
  catch ( const NoRouteException & e )
  {
  }

  list<string> r11 = lines . Find ( "Salzburg", "Salzburg" );
  assert ( toText ( r11 ) == "Salzburg" );

  list<string> r12 = lines . Find ( "Salzburg", "Salzburg", [] ( const CTrain & x ) { return x . m_Company == "SNCF"; }  );
  assert ( toText ( r12 ) == "Salzburg" );

  try
  {
    list<string> r13 = lines . Find ( "London", "Oxford" );
    assert ( "London > Oxford connection does not exist!!" == NULL );
  }
  catch ( const NoRouteException & e )
  {
  }

    CRoute<int,CTrain> linesInts;

    linesInts . Add ( 2, 3, CTrain ( "DB", 120 ) )
            . Add ( 1, 5, CTrain ( "CD",  80 ) )
            . Add ( 3, 1, CTrain ( "DB", 160 ) );

    list<int> alt = linesInts . Find ( 1, 2 );
    list<int> ref = {1,3,2};
    assert ( ref == alt );


  return 0;
}
#endif  /* __PROGTEST__ */
